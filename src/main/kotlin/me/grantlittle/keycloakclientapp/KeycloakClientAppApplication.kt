package me.grantlittle.keycloakclientapp

import org.keycloak.KeycloakPrincipal
import org.keycloak.KeycloakSecurityContext
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.repository.CrudRepository
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.session.SessionRegistryImpl
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import java.security.Principal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.servlet.http.HttpServletRequest





@SpringBootApplication
class KeycloakClientAppApplication {

}

@Entity
class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0
    var name: String? = null
    var serviceRendered: String? = null
    var address: String? = null

}

@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = [KeycloakSecurityComponents::class])
internal class SecurityConfig : KeycloakWebSecurityConfigurerAdapter() {

    // Submits the KeycloakAuthenticationProvider to the AuthenticationManager
    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        val keycloakAuthenticationProvider = keycloakAuthenticationProvider()
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(SimpleAuthorityMapper())
        auth.authenticationProvider(keycloakAuthenticationProvider)
    }

    @Bean
    fun KeycloakConfigResolver(): KeycloakSpringBootConfigResolver {
        return KeycloakSpringBootConfigResolver()
    }

    // Specifies the session authentication strategy
    @Bean
    override fun sessionAuthenticationStrategy(): SessionAuthenticationStrategy {
        return RegisterSessionAuthenticationStrategy(SessionRegistryImpl())
    }

    override fun configure(http: HttpSecurity) {
        super.configure(http)
        http.authorizeRequests()
            .antMatchers("/customers*", "/users*")
            .hasRole("user")
            .anyRequest()
            .permitAll()
    }
}

interface CustomerDAO : CrudRepository<Customer?, Long?>


@Controller
class CustomUserAttrController {
    @GetMapping(path = ["/users"])
    fun getUserInfo(model: Model): String {
        val authentication = SecurityContextHolder.getContext()
            .getAuthentication() as KeycloakAuthenticationToken
        val principal: Principal = authentication.principal as Principal
        var dob = ""
        if (principal is KeycloakPrincipal<*>) {
            val kPrincipal = principal as KeycloakPrincipal<KeycloakSecurityContext>
            val token = kPrincipal.keycloakSecurityContext
                .idToken
            val customClaims = token.otherClaims
            if (customClaims.containsKey("DOB")) {
                dob = customClaims["DOB"].toString()
            }
        }
        model.addAttribute("username", principal.getName())
        model.addAttribute("dob", dob)
        return "userInfo"
    }
}

@Controller
class WebController {
    @Autowired
    private val customerDAO: CustomerDAO? = null
    @GetMapping(path = ["/"])
    fun index(): String {
        return "external"
    }

    @GetMapping("/logout")
    fun logout(request: HttpServletRequest): String {
        request.logout()
        return "redirect:/"
    }

    @GetMapping(path = ["/customers"])
    fun customers(principal: Principal, model: Model): String {
        addCustomers()
        val customers = customerDAO!!.findAll()
        model.addAttribute("customers", customers)
        model.addAttribute("username", principal.name)
        return "customers"
    }

    // add customers for demonstration
    fun addCustomers() {
        val customer1 = Customer()
        customer1.address = "1111 foo blvd"
        customer1.name = "Foo Industries"
        customer1.serviceRendered = "Important services"
        customerDAO!!.save(customer1)
        val customer2 = Customer()
        customer2.address = "2222 bar street"
        customer2.name = "Bar LLP"
        customer2.serviceRendered = "Important services"
        customerDAO.save(customer2)
        val customer3 = Customer()
        customer3.address = "33 main street"
        customer3.name = "Big LLC"
        customer3.serviceRendered = "Important services"
        customerDAO.save(customer3)
    }
}

fun main(args: Array<String>) {
    runApplication<KeycloakClientAppApplication>(*args)
}
