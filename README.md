# Getting Started

## Services

### Docker
```shell
docker network create --driver=bridge keycloak-network
docker build -t keycloak-haproxy --rm .
docker run --net keycloak-network --name keycloak -d -e KEYCLOAK_USER=admin -e PROXY_ADDRESS_FORWARDING=true -e KEYCLOAK_PASSWORD=admin --rm grantlittle/keycloak:15.0.2
docker run --net keycloak-network -p 9090:9090 --rm -d --name haproxy keycloak-haproxy
```

### Podman
```shell
podman pod create --name keycloak-pod -p 127.0.0.1:9090:9090
docker build -t keycloak-haproxy --rm .
podman run --pod keycloak-pod --name keycloak -d -e KEYCLOAK_USER=admin -e PROXY_ADDRESS_FORWARDING=true -e KEYCLOAK_PASSWORD=admin --rm grantlittle/keycloak:15.0.2
podman run --pod keycloak-pod --rm -d --name haproxy keycloak-haproxy
```

## Keycloak Configuration

[Follow the Keycloak configuration instructions here](https://www.baeldung.com/spring-boot-keycloak)

## Hit URL

[Application URL](http://localhost:9090)